# Interview Questions

* Name (pronunciation) (if not sure) How do I pronounce your name?

## Homework	
* What do you know about OpenDrives?
*	What do you know about NAS
*	What do you know about M&E (Media and Entertainment)? HPC?

## Basic Technical Knowledge for Full Stack Developers
* Have you ever used NodeJS? ExpressJS? PassportJS? CoteJS?
*	Monolith vs Microservices? Benefits and Drawbacks of each?
*	ES5? ES6? ES11? What do you like about ES6 compared to ES5? Current ES version? 12?
*	Created an SPA? React? Vue? Angular? Svelte? Which do you prefer and why?
*	Do you know anything about Electron, Neutrino, Ionic, or React Native?
*	SQL? NoSQL? How do you interact with a database normally?
*	Shell? Bash? Makefiles?
*	Have you ever used Linux? Linux terminal commands? and Git commands?

## Basic Software Development
* Do you know what SDLC stands for?
*	Have you worked in a team? What's the biggest/smallest team you've worked on?
*	Have you used Jira, Trello, Monday, or another project planning software?
*	Have you ever written code to test your code?
*	Have you ever worked in a distributed environment?

## Soft Skills
*  How do you get your best work done? What makes an exceptional day? Think about the best day you had and how it was different from other days. Describe the work environment or culture in which you are most productive (and happy).
*	What are you three best qualities as a developer?
*	What are the three most important personal qualities that you cultivate in yourself? (ASK SLOWLY AND SINCERELY)
*	What are you working on right now?
*	How did you become a developer?
*	What do you do when you make a mistake?
*	Do you have any questions?
*	What are you passionate about?
*	What did you like most and least about your current/last company?
*	How do you handle your stress?
*	Tell me about your dream job, team, and company
*	What interests you about this position you applied

## Comment
* What was the most difficult problem you encountered? and how did you solve this problem?
*	Is there a project that you are most proud of? What is it? and what did you contribute on this project?
